package com.javabasic;

/**Class Application operates with two classes.
 * They allow to work with Fibonacci numbers
 * in different way
 *
 * @author Yullia Kurylchyk
 * @version 1.0
 * @since 2019-11-08
 */

public final class Application {

    /**
     * Function creates objects of two classes.
     * {@link FibonacciNumber} and {@link FibonacciSet}.
     * Could take some params
     * @param args -  string values
     */
    public static void main(final String[] args) {

        FibonacciNumber fibonacciNumber = new FibonacciNumber();
        fibonacciNumber.printOddNumbers();
        fibonacciNumber.printEvenNumbers();
        System.out.println(fibonacciNumber.getSumOfOddAndEvenNumbers());
        fibonacciNumber.printOddFibonacci();
        fibonacciNumber.printEvenFibonacci();

        FibonacciSet fibonacciSet = new FibonacciSet();
        fibonacciSet.printFibonacciSet();
        fibonacciSet.getPercentage();
    }

    /**
     * private constructor as this class is utility.
     */
    private Application() {

    }
}
