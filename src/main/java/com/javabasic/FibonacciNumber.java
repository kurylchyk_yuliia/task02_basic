package com.javabasic;

import java.util.Scanner;

/**
 * class FibonacciNumber provides user to set  the interval.
 * {@link FibonacciNumber#getInterval()}
 * and shows odd and even of Fibonacci numbers
 */

public class FibonacciNumber {

    /**
     * maximum value of interval.
     */
    private int maxValue;
    /**
     * minimal value of interval.
     */
    private int minValue;

    /**
     * sum of odd numbers only of  the interval.
     */
    private int sumOfOddNumbers;
    /**
     * sum of even numbers only of the  interval.
     */
    private int sumOfEvenNumbers;
    /**
     * the biggest odd number of interval.
     */
    private int theBiggestOddNumber;

    /**
     * the biggest even number of interval.
     */
    private int theBiggestEvenNumber;

    /**
     * Construstor sets default values  and calls the function.
     *
     * {@link FibonacciNumber#getInterval()}
     * {@link FibonacciNumber#getTheBiggestValues()}
     */
    FibonacciNumber() {
        sumOfOddNumbers = 0;
        sumOfEvenNumbers = 0;
        getInterval();
        getTheBiggestValues();

    }

    /**
     * Prints only odd numbers from interval.
     *
     * @see FibonacciNumber#getInterval()
     */

    public final void printOddNumbers() {


        System.out.print("\nThe odd numbers: ");
        int index = minValue;


        while (index <= maxValue) {

            if (index % 2 != 0) {
                System.out.print(index + "\t");
                sumOfOddNumbers += index;
            }


            index++;
        }

    }


    /**
     * Prints only even numbers from interval.
     *
     * @see FibonacciNumber#getInterval()
     */
    public final void printEvenNumbers() {
        int index = maxValue;

        System.out.print("\nThe even numbers: ");

        while (index >= minValue) {

            if (index % 2 == 0) {
                System.out.print(index + "\t");
                sumOfEvenNumbers += index;
            }
            index--;
        }
    }


    /**
     * @return the sum of both odd and even numbers of interval.
     * {@link FibonacciNumber#sumOfEvenNumbers}
     * {@link FibonacciNumber#sumOfOddNumbers}
     */
    public final int getSumOfOddAndEvenNumbers() {


        System.out.print("\nThe sum of even and odd numbers: ");
        return sumOfOddNumbers + sumOfEvenNumbers;

    }

    /**
     * Asks user to set the interval.
     * Interval from:
     * {@link FibonacciNumber#minValue}
     * to
     * {@link FibonacciNumber#maxValue}
     */

    private void getInterval() {


        int min = 0;
        int max = 0;

        while (min >= max) {
            Scanner sc = new Scanner(System.in);
            System.out.print("\nEnter the minimum value: ");
            min = sc.nextInt();

            System.out.print("\nEnter the maximum value: ");
            max = sc.nextInt();
        }
        maxValue = max;
        minValue = min;
    }

    /**
     * Sets the biggest odd and even values of interval.
     * {@link FibonacciNumber#theBiggestEvenNumber}
     * {@link FibonacciNumber#theBiggestOddNumber}
     */
    private void getTheBiggestValues() {

        if (maxValue % 2 == 0) {
            theBiggestEvenNumber = maxValue;
            theBiggestOddNumber = maxValue - 1;
        } else {
            theBiggestEvenNumber = maxValue - 1;
            theBiggestOddNumber = maxValue;
        }

    }


    /**
     * Prints odd Fibonacci numbers of the interval.
     *
     * @see FibonacciNumber#getInterval() ;
     */


    public final void printOddFibonacci() {
        System.out.print("\nOdd Fibonacci numbers: ");
        int index = minValue;
        while (index <= theBiggestOddNumber) {
            if (index % 2 != 0
                    && ((index == (index - 1) + (index - 2)) || index == 1)) {
                              System.out.print(index + "\t");
            }

            index++;
        }
    }


    /**
     * Prints even Fibonacci numbers of the interval.
     *
     * @see FibonacciNumber#getInterval() ;
     */

    public final void printEvenFibonacci() {

        System.out.print("\nEven Fibonacci numbers: ");
        int index = minValue;
        while (index <= theBiggestEvenNumber) {
            if (index % 2 == 0
                    && (index == (index - 1) + (index - 2)) || index == 2) {
                System.out.print(index + "\t");
            }
            index++;
        }
    }


}

