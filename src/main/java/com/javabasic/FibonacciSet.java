package com.javabasic;

import java.util.Scanner;
/**
 * class FibonacciSet generates Fibonacci numbers.
 * It also calculates odd and even percentage of the set
 * accorging to the size of this set {@link FibonacciSet#sizeOfSet}
 *
 */
public class FibonacciSet {

    /**
     * Constant value.
     * Used to get frequency of occurrence
     * in percentage appearance
     */
   private final int PERCENT = 100;

    /**
     * count of elements of Fibonacci numbers.
     */
    private int sizeOfSet = 0;
    /**
     * array with size {@link FibonacciSet#sizeOfSet}.
     * The array contains Fibonacci elements.
     */
    private int[] fibonacciNumber;
    /**
     * contains the frequency of occurrence odd elements in array.
     * {@link FibonacciSet#fibonacciNumber}.
     */
    private int countOfOdd;
    /**
     * contains the frequency of occurrence even elements in array.
     * {@link FibonacciSet#fibonacciNumber}.
     */
    private int countOfEven;



    /**
     * Constructor sets the default values and calls functions.
     * @see FibonacciSet#setSizeOfSet()
     * @see FibonacciSet#buildFibonacci()
     * and fills the array with values
     */
    FibonacciSet() {
        countOfEven = 0;
        countOfOdd = 0;


        setSizeOfSet();
        fibonacciNumber = new int[sizeOfSet];
        buildFibonacci();
    }

    /**
     * Set the size.
     * {@link FibonacciSet#sizeOfSet}
     */
    private void setSizeOfSet() {
        Scanner sc = new Scanner(System.in);
        while (sizeOfSet <= 0) {
            System.out.println("\nPlease, enter the size of set");
            sizeOfSet = sc.nextInt();
        }
    }

    /**
     * Fill the array {@link FibonacciSet#fibonacciNumber}.
     * Values are created according to the Fibonacci formula.
     */
    private void buildFibonacci() {


        for (int index = 0; index < sizeOfSet; index++) {
            if (index == 0 || index == 1) {
                fibonacciNumber[index] = 1;
            } else {
                fibonacciNumber[index] =
                        fibonacciNumber[index - 1] + fibonacciNumber[index - 2];
            }
        }
    }


    /**
     * Print the values of {@link FibonacciSet#fibonacciNumber}.
     */
    public final void printFibonacciSet() {
        System.out.print("\nFibonacci set: ");
        for (int index = 0; index < sizeOfSet; index++) {
            System.out.print(fibonacciNumber[index] + "\t");
        }
    }


    /**
     * Returns const value.
     * @return PERCENT value
     *
     */

    private  int  getPercentValue() {

        return PERCENT;
    }


    /**
     * Calculate the frequency of odd and even elements in array.
     * {@link FibonacciSet#fibonacciNumber}
     */
    private void getCountOfOddAndEven() {
        for (int index = 0; index < sizeOfSet; index++) {
            if (fibonacciNumber[index] % 2 != 0) {
                countOfOdd++;
            } else {
                countOfEven++;
            }
        }

    }


    /**
     * Calculate the percentage.
     * Method uses
     * {@link FibonacciSet#countOfOdd}
     * {@link FibonacciSet#countOfEven}
     * {@link FibonacciSet#sizeOfSet}
     */
    public final void getPercentage() {
        
        getCountOfOddAndEven();
        System.out.println("\nOdd numbers in list: "
                + (((float) countOfOdd / sizeOfSet)) * getPercentValue() + "%");
        System.out.println("Even numbers in list: "
                + (((float) countOfEven / sizeOfSet))
                    * getPercentValue() + "%");

    }
}
